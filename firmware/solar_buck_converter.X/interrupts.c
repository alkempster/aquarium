#include <p18f4520.h>

volatile unsigned char ms_count = 0x0;

void interrupt high_isr(void)
{
    if(INTCONbits.TMR0IF)
    {
        
    }
}

void interrupt low_priority low_isr(void)
{
    /* Interrupt every 1ms
     * This section updates ms_count once every millisecond such that
     * other parts of the firmware can read ms_count to measure
     * elapsed time
     */
    if(INTCONbits.TMR0IF)
    {
        ms_count++;                 // increment ms count

        TMR0 = -25;                 // reload the timer
        INTCONbits.TMR0IF = 0x0;    // Reset interrupt
    }
}
