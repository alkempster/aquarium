/*
 * aquarium software tool
 * Alex Kempster
 * 27.10.2013
 */

#include "general.h"

void callback_new_aquarium_item(void);
void callback_quit_item(void);

extern GtkWidget *window_main;
extern GtkWidget *window_main_vbox;
extern GtkWidget *main_root_menu;
extern GtkWidget *main_menu_bar;
    extern GtkWidget *file_menu;

extern GtkWidget *file_menu;
extern GtkWidget *new_aquarium_item;
extern GtkWidget *quit_item;

extern char program_continue;

inline void main_window_init(void)
{
    /* create the main window */
    window_main = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (window_main), 256);
    gtk_window_set_title (GTK_WINDOW (window_main), "Aquarium Control Center");
    gtk_window_set_position (GTK_WINDOW (window_main), GTK_WIN_POS_CENTER);
    gtk_widget_realize (window_main);
    g_signal_connect (window_main, "destroy", gtk_main_quit, NULL);

    file_menu = gtk_menu_new();         /* create a menu */
        new_aquarium_item  = gtk_menu_item_new_with_label("New Aquarium");
        quit_item = gtk_menu_item_new_with_label("Quit");

    /* Add menu items to the menu */
    gtk_menu_append(GTK_MENU(file_menu), new_aquarium_item);
    gtk_menu_append(GTK_MENU(file_menu), quit_item);

    /* Attatch callback functions to each menu item */
    gtk_signal_connect_object(GTK_OBJECT(new_aquarium_item), "activate", GTK_SIGNAL_FUNC(callback_new_aquarium_item), NULL);
    gtk_signal_connect_object(GTK_OBJECT(quit_item), "activate", GTK_SIGNAL_FUNC(callback_quit_item), NULL);
    gtk_widget_show(new_aquarium_item);
    gtk_widget_show(quit_item);

    main_root_menu = gtk_menu_item_new_with_label("Add");
    gtk_widget_show(main_root_menu);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(main_root_menu), file_menu);

    window_main_vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window_main), window_main_vbox);
    gtk_widget_show(window_main_vbox);

    main_menu_bar = gtk_menu_bar_new();
    gtk_box_pack_start(GTK_BOX(window_main_vbox), main_menu_bar, FALSE, FALSE, 2);
    gtk_widget_show(window_main_vbox);

    gtk_menu_bar_append(GTK_MENU_BAR(main_menu_bar), main_root_menu);

    /* Display the window and everythign inside it */
    gtk_widget_show_all (window_main);
}

void callback_new_aquarium_item(void)
{
    g_message("testing: new pressed\n");
}

void callback_quit_item(void)
{
    program_continue = 0x0;     // Close down none gtk aspects of the program
    gtk_main_quit();
}
