/* Header files */
#include "p18f4520.h"
#include "lcd.h"

extern volatile unsigned char ms_count;

/* Writes a character to a specific location on the lcd display */
void lcd_write_char(char data, char row, char column)
{
    const char address[4][20] =
        {{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,//l1
                0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13},
         {0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A,//l2
                0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53},
         {0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E,//l3
                0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27},
         {0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E,//l4
                0x5F, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67}};

    /* assumes values start in row 1 column 1, adjust for array location */
    row--;
    column--;

    if (row > 4 || column > 20) // error with the entry
        return;

    LCD_RS = 0x0;                           // set DDRAM address
    LCD_RW = 0x0;
    LCD_DATA = 0x80 | address[row][column]; // Address for row and column

    LCD_ENABLE = 0x1;   // Toggle enable pin
    NOP();NOP();NOP();
    LCD_ENABLE = 0x0;

    wait_ms(2);

    LCD_RS = 0x1;       // send character
    LCD_RW = 0x0;
    LCD_DATA = data;    // Address for row and column

    LCD_ENABLE = 0x1;   // Toggle enable pin
    NOP();NOP();NOP();
    LCD_ENABLE = 0x0;

    wait_ms(2);
}

/*
 * uses ms_count to wait for an elapsed period of time.
 * It will wait at least the required ms but may wait longer by up to a ms
 * to allow the function to correctly syncronise
 */
void wait_ms(unsigned char value)
{
    unsigned char count = 0x0;
    
    if (ms_count >= (255-(value+1)))   // if counter will overflow
        count = (value+1)-(255 - ms_count);    // account for overflow
    else
        count = ms_count + (value+1);  // load target value

    while (count != ms_count);  // wait for count to finish
}

/*
 * Initialise the lcd module
 */
void lcd_init(void)
{    
    #ifdef LCD_MC42004A6W_FPTLW_4_20_MIDAS
        TRISD = 0x0;    // DB<0:7> as outputs
        LCD_DATA = 0x0;    // Clear DB<0:7>

        TRISCbits.RC0 = 0x0;    // LCD control lines
        TRISCbits.RC1 = 0x0;
        TRISCbits.RC2 = 0x0;
        TRISBbits.RB3 = 0x0;

        /* Initialise lcd */   
        wait_ms(15);        // Wait 15ms

        LCD_RS = 0x0;       // Function set instruction
        LCD_RW = 0x0;
        LCD_DATA = 0x38;    // 8-bit, 2 line display mode, 5x8 dot format

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(5);         // > 4.1ms

        LCD_RS = 0x0;       // Function set instruction
        LCD_RW = 0x0;
        LCD_DATA = 0x38;    // 8-bit, 2 line display mode, 5x8 dot format

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(1);         // > 100Us

        LCD_RS = 0x0;       // Function set instruction
        LCD_RW = 0x0;
        LCD_DATA = 0x38;    // 8-bit, 2 line display mode, 5x8 dot format

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // Function set instruction
        LCD_RW = 0x0;
        LCD_DATA = 0x38;    // 8-bit, 2 line display mode, 5x8 dot format

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // Display off
        LCD_RW = 0x0;
        LCD_DATA = 0x0B;    // display off, cursor on, blinking

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // Display clear
        LCD_RW = 0x0;
        LCD_DATA = 0x01;

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // Entry mode set
        LCD_RW = 0x0;
        LCD_DATA = 0x06;    // move cursor to right, do not shift the display

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // Turn on display
        LCD_RW = 0x0;
        LCD_DATA = 0x0F;

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms

        LCD_RS = 0x0;       // set DDRAM to zero
        LCD_RW = 0x0;
        LCD_DATA = 0x80;

        LCD_ENABLE = 0x1;   // Toggle enable pin
        NOP();NOP();NOP();
        LCD_ENABLE = 0x0;

        wait_ms(2);         // wait 2ms
    #endif
}
