/* 
 * File:   lcd.h
 * Author: alex
 *
 * Created on 08 October 2013, 21:53
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C" {
#endif

/* Function prototype declarations */
void lcd_init(void);
void wait_ms(unsigned char value);
void lcd_write_char(char data, char row, char column);

/* Definitions */
#define LCD_MC42004A6W_FPTLW_4_20_MIDAS         // Farnell: 206-3174

#define LCD_SOLAR

#ifdef LCD_SOLAR
    #define LCD_DATA        PORTD
    #define LCD_RS          PORTCbits.RC0
    #define LCD_RW          PORTCbits.RC1
    #define LCD_CONTRAST    PORTBbits.RB3
    #define LCD_ENABLE      PORTCbits.RC2
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

