/*
 * aquarium software tool
 * Alex Kempster
 * 21.10.2013
 *
 * Contains all global variables that are not local to any source file
 */

/* Header files for variable definitions */
#include <gtk/gtk.h>

/* OpenMp variables */
char program_continue = 0x1;        // Keeps track of wether GTK has closed, if it does then the program should close

/* GTK variables */
GtkWidget *window_main      = NULL;
GtkWidget *window_main_vbox = NULL;
GtkWidget *main_root_menu   = NULL;
GtkWidget *main_menu_bar    = NULL;
    GtkWidget *file_menu    = NULL;
    GtkWidget *new_aquarium_item     = NULL;
    GtkWidget *quit_item    = NULL;
