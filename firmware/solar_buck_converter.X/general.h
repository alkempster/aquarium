/* 
 * File:   general.h
 * Author: alex
 *
 * Created on 06 October 2013, 21:55
 */

#ifndef GENERAL_H
#define	GENERAL_H

/* MICROCONTROLLER PINOUT
 * +-----+-------------+-----+-------------+
 * | PIN | DESCRIPTION | PIN | DESCRIPTION |
 * +-----+-------------+-----+-------------+
 * |  01 | !MCLRE      |  06 |             |
 * |  02 | Solar Vref  |  07 |             |
 * |  03 | Batt Vref   |  08 |             |
 * |  04 |             |  09 |             |
 * |  05 |             |  10 |             |
 * +-----+-------------+-----+-------------+
 * |  11 | Vdd +5.0v   |  16 | LCD_RW      |
 * |  12 | Vss         |  17 | LCD_E       |
 * |  13 |             |  18 |             |
 * |  14 |             |  19 | RD0 -> RB0  |
 * |  15 | LCD_RS      |  20 | RD1 -> RB1  |
 * +-----+-------------+-----+-------------+
 * |  21 | RD2 -> RB2  |  26 |             |
 * |  22 | RD3 -> RB3  |  27 | RD4 -> RB4  |
 * |  23 |             |  28 | RD5 -> RB5  |
 * |  24 |             |  29 | RD6 -> RB6  |
 * |  25 |             |  30 | RD7 -> RB7  |
 * +-----+-------------+-----+-------------+
 * |  31 | Vss         |  36 | LCD CONTRAST|
 * |  32 | Vdd +5.0v   |  37 |             |
 * |  33 |             |  38 | PGM         |
 * |  34 |             |  39 | PGC         |
 * |  35 |             |  40 | PGD         |
 * +-----+-------------+-----+-------------+
 */

/* prototype declarations specific to the project */
void init(void);
void init_timer0(void);

inline float calc_vdivider(int r1, int r2, float vout);

/* definitions */
#define CLOCK     8000000

#define TRUE    0x1
#define FALSE   0x0

#endif	/* GENERAL_H */

