/*
 * aquarium software tool
 * Alex Kempster
 * 21.10.2013
 */

/* Header files */
#include "general.h"    // contains general information and files required by all files

/* global variables - see gvars.c */
extern GtkWidget *window_main;
extern char program_continue;

/* starts up the gtk portion of the program */
void init_gtk(int argc, char *argv[])
{
    /* Initialize GTK+ */
    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, (GLogFunc) gtk_false, NULL);
    gtk_init (&argc, &argv);
    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, g_log_default_handler, NULL);

    main_window_init();     // Create the main window

    /* Enter the main loop */
    gtk_main ();
}

/* Program starts from here */
int main (int argc, char *argv[])
{
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            init_gtk(argc, argv);   // all gtk code is stored in here
        }

        #pragma omp section
        {
            while (program_continue)    // run until program asked to close
                background();
        }
    }

  return 0;
}

void background(void)
{

}
