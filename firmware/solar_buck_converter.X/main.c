/* 
 * File:   main.c
 * Author: alex
 *
 * Created on 06 October 2013, 21:18
 */

/* Header files */
#include <p18f4520.h>
#include "general.h"
#include "lcd.h"

/* Configuration bits */
#pragma config OSC=INTIO67      // Internal oscillator
#pragma config FCMEN=OFF        // Fail safe clock monitor
#pragma config IESO=OFF         // Disable clock switching
#pragma config PWRT=ON          // Enable power up timer
#pragma config BOREN=ON         // Enable brown out detection
    #pragma config BORV=3       // Minimum brownout setting
#pragma config WDT=OFF          // Disable watch dog
    //#pragma config WDTPS=1
#pragma config CCP2MX=PORTC     // CCP2 multiplexed with RC1
#pragma config PBADEN=OFF       // PORTB<4:0> digital IO on reset
#pragma config LPT1OSC=ON       // Timer1 configured for low power
#pragma config MCLRE=ON         // Enable MCLRE pin
#pragma config STVREN=ON        // Stack over/under flow will cause reset
#pragma config LVP=OFF          // Single supply ICSP disabled
#pragma config XINST=OFF        // extended instruction set
#pragma config CP0=OFF          // Code protection
#pragma config CP1=OFF
#pragma config CP2=OFF
#pragma config CP3=OFF
#pragma config CPB=OFF
#pragma config CPD=OFF
#pragma config WRT0=OFF         // Write protection
#pragma config WRT1=OFF
#pragma config WRT2=OFF
#pragma config WRT3=OFF
#pragma config WRTC=OFF
#pragma config WRTB=OFF
#pragma config WRTD=OFF
#pragma config EBTR0=OFF        // Table read protection
#pragma config EBTR1=OFF
#pragma config EBTR2=OFF
#pragma config EBTR3=OFF
#pragma config EBTRB=OFF

/*
 * Program starts here
 */
void main(void)
{
    init();
    init_timer0();
    lcd_init();

    // Line 1
    lcd_write_char(0x48,1,1);   // H
    lcd_write_char(0x65,1,2);   // e
    lcd_write_char(0x6C,1,3);   // l
    lcd_write_char(0x70,1,4);   // p

    // arrow on each start of line
    lcd_write_char(0x7E,2,1);   // ->
    lcd_write_char(0x7E,3,1);   // ->
    lcd_write_char(0x7E,4,1);   // ->

    while (TRUE)
    {

    }
}

/*
 * init_timer0
 * sets up timer 0 to interrupt every 1ms interval
 *
 * 8Mhz ticks every 1/8000000 = 0.000000125 seconds 0.000125 mS or 125uS
 * 1ms = 1/0.00125 = 800 ticks
 */
void init_timer0(void)
{
    INTCONbits.GIE     = 0x1;    // Enable global interrupts
    INTCONbits.PEIE    = 0x1;    // Enable peripheral interrupts
    RCONbits.IPEN      = 0x1;    // Enable interrupt priority
    INTCON2bits.TMR0IP = 0x0;    // timer 0 as low priority
    INTCONbits.TMR0IF  = 0x0;    // Clear timer 0 interrupt flag
    INTCONbits.TMR0IE  = 0x1;    // Enable timer 0 interrupts

    TMR0 = -25;     // up count, interrupt on overflow
    T0CON = 0xC4;   // on, 8-bit mode, 1:32 prescale -> 25 ticks = 1ms
}

/*
 * Calculate the voltage represented by a resistive voltage divider circuit
 * This is used to convert the representative voltage to the origonal voltage
 * value. It does not convert AD peripheral values to voltages.
 *
 * Vout = (r2/(r1+r2)) * V                    <- potential divider calculation
 *
 * V = Vout/(r2/(r1+r2))                      <- Rearrange to find input voltage
 *
 * r1   - resistor connected from voltage to Vout
 * r2   - resistor connected from Vout to ground
 * V    - Voltage which the divider is based from
 * Vout - Output from the voltage divider circuit
 */
inline float calc_vdivider(int r1, int r2, float vout)
{
    float voltage;

    voltage = vout/(r2/(r1+r2));

    return voltage;
}

/*
 * Initialize the microcontroller
 */
void init(void)
{
    /* Pin directions */
    TRISA = 0x1;    // AN0 as an input
    TRISB = 0x0;
    TRISC = 0x0;
    TRISD = 0x0;    // whole port is output for DB<0:7> of LCD
    TRISE = 0x0;

    /* Pin values */
    PORTA = 0x0;
    PORTB = 0x0;
    PORTC = 0x0;
    PORTD = 0x0;    // default LCD data as nothing
    PORTE = 0x0;

    /* Analogue settings
     * AN0 is used for voltage reading from the battery charge input.
     * The voltage can vary between 4v-30v. AN0 has a 150K:750K voltage
     * divider circuit. This provides 2.5v input at 15v and 5v input at 30v
     * Typically the input for battery charging should be 14.8v in order to
     * charge the connected 12v lead-acid batteries. Conversion does not have
     * to be fast, slower conversion = higher efficiency & accuracy
     */
// May need to re-arrange after device set up
    ADCON0 = 0x0;   // Select AN0, do not start sampling
    ADCON1 = 0x0E;  // Set AN0 as the only analogue, Vrefs taken from Vss & Vdd
    ADCON2 = 0x39;  // Left justified (loose 2LSB), 20 Tad, Fosc/8

}
