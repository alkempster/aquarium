/*
 * aquarium software tool
 * Alex Kempster
 * 21.10.2013
 */

#include <gtk/gtk.h>
#include <omp.h>        // parallel programming

 /* Header files for each source file */
 #include "main_window.h"

/* Function prototype declarations */
void init_gtk(int argc, char *argv[]);  // main.c
void background(void);
